<?php

/**
 * @file
 *
 * The Checkbox Auto Create Widget.
 */

namespace Drupal\checkbox_auto_create\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsButtonsWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;

/**
 * Plugin implementation of the 'checkbox_auto_create' widget.
 *
 * @FieldWidget(
 *   id = "checkbox_auto_create",
 *   label = @Translation("Checkbox auto create term"),
 *   field_types = {
 *     "taxonomy_term_reference"
 *   },
 *   multiple_values = TRUE
 * )
 */
class CheckboxAutoCreateWidget extends OptionsButtonsWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['checkboxes'] = parent::formElement($items, $delta, $element, $form, $form_state);
    $element['checkboxes']['#required'] = FALSE;
    unset($element['checkboxes']['#title']);

    $element += array(
      '#type' => 'fieldset',
      '#id' => 'checkboxes-replace--wrapper',
      '#tree' => TRUE,
    );

    $storage = $form_state->getStorage();
    if (isset($storage['new_terms'])) {

      // Add term to existing terms.
      $terms = $this->getFieldTerms($form_state);
      foreach ($storage['new_terms'] as $index => $new_term) {
        $terms['new_term_' . $index] = $new_term;
      }
      $element['checkboxes']['#options'] = $terms;

      // Set new term default on.
      $user_input = $form_state->getUserInput();
      $terms_on = $user_input[$this->getFieldName()]['checkboxes'];
      $terms_on = array_filter($terms_on);

      $last_term_key = end(array_keys($terms));
      $terms_on[$last_term_key] = $last_term_key;

      // #default_value is ignored in ajax callbacks,
      // use #value instead.
      $element['checkboxes']['#value'] = $terms_on;
    }

    $element['new_term'] = array(
      '#type' => 'details',
      '#title' => t('Add new term'),
      '#open' => TRUE,
      '#attributes' => array('class' => array('fieldgroup', 'form-composite')),
      '#validate' => array(array($this, 'validateCheckboxAutoCreate')),
    );

    $element['new_term']['new_term_name'] = array(
      '#type' => 'textfield',
      '#title' => t('New term'),
      '#maxlength' => $this->getFieldSetting('max_length'),
      '#weight' => 5,
      // #default_value is ignored in ajax callbacks,
      // use #value instead.
      '#value' => '',
      '#element_validate' => array(array($this, 'validateNewTerm')),
    );

    $element['new_term']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add term'),
      '#weight' => 6,
      '#submit' => array(array($this, 'addTermSubmit')),
      '#ajax' => array(
        'callback' => array($this, 'addTermCallback'),
        'wrapper' => array('checkboxes-replace--wrapper'),
        'effect' => 'fade',
        'method' => 'replace',
      ),
    );

    return $element;
  }

  /**
   * Get all terms of this field.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *    Form state.
   *
   * @return mixed
   *    List of terms.
   */
  private function getFieldTerms(FormStateInterface $form_state) {
    $complete_form = $form_state->getCompleteForm();
    $checkboxes = $complete_form[$this->getFieldName()]['widget']['checkboxes'];

    return $checkboxes['#options'];
  }

  /**
   * Get the name of the current field.
   *
   * @return string
   *   Name of the current field.
   */
  private function getFieldName() {
    return $this->fieldDefinition->getName();
  }

  /**
   * Set new term in form_state storage, ready to be saved.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function addTermSubmit(array &$form, FormStateInterface &$form_state) {
    $new_term = $this->getNewTerm($form_state);

    $storage = $form_state->getStorage();
    $storage['new_terms'][] = $new_term;
    $form_state->setStorage($storage);
    $form_state->setRebuild(TRUE);
  }

  /**
   * Get new term from the form_state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return string
   *   Term name.
   */
  private function getNewTerm(FormStateInterface $form_state) {
    $form_values = $form_state->getUserInput();

    return $form_values[$this->getFieldName()]['new_term']['new_term_name'];
  }

  /**
   * Add term on to form on callback.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return mixed
   *   Element.
   */
  public function addTermCallback(array &$form, FormStateInterface &$form_state) {
    $button = $form_state->getTriggeringElement();
    $parent = array_slice($button['#array_parents'], 0, -2);
    $element = NestedArray::getValue($form, $parent);

    return $element;
  }

  /**
   * Process new terms.
   *
   * @param array $values
   *   Values.
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return array
   *   Array containing all checked terms.
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $user_input = $form_state->getUserInput();
    $terms_on = $user_input['field_ingredients']['checkboxes'];
    $terms_on = array_filter($terms_on);

    $items = array();
    foreach ($terms_on as $term_on) {
      $items[] = $term_on;
    }

    // Get all terms from form.
    $terms = $this->getFieldTerms($form_state);

    // Autocomplete widgets do not send their tids in the form, so we must
    // detect them here and process them independently.
    $items = array();

    // Collect candidate vocabularies.
    foreach ($this->getFieldSetting('allowed_values') as $tree) {
      if ($vocabulary = entity_load('taxonomy_vocabulary', $tree['vocabulary'])) {
        $vocabularies[$vocabulary->id()] = $vocabulary;
      }
    }

    $items_target_id = array();

    // Translate term names into actual terms.
    foreach ($terms_on as $value) {
      // See if the term exists in the chosen vocabulary and return the tid;
      // otherwise, create a new term.
      if ($possibilities = entity_load_multiple_by_properties('taxonomy_term', array(
        'name' => trim($terms[$value]),
        'vid' => array_keys($vocabularies),
      ))
      ) {
        $term = array_pop($possibilities);

        // Make sure there are no duplicates by using
        // term id as key in temp array.
        $items_target_id[$term->id()] = $term->id();
      }
      else {
        $vocabulary = reset($vocabularies);
        $term = entity_create('taxonomy_term', array(
          'vid' => $vocabulary->id(),
          'name' => $terms[$value],
        ));
        $items[] = array('entity' => $term);
      }
    }

    // Add existing checked items to the list.
    foreach ($items_target_id as $target_id) {
      $items[] = array('target_id' => $target_id);
    }

    return $items;
  }

  /**
   * Validate the field.
   *
   * @param string $element
   *   Element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   * @param array $form
   *   Form.
   */
  public function validateNewTerm($element, FormStateInterface $form_state, array $form) {
    $triggered_element = $form_state->getTriggeringElement();
    $new_term = $this->getNewTerm($form_state);

    if (isset($triggered_element['#ajax']) && empty($new_term)) {
      $form_state->setError($element, t('!name field is required.', array('!name' => $element['#title'])));
    }
  }

}
