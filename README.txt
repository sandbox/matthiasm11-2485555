Provides a widget for taxonomy fields with additional option
to create new terms on the fly.

- New checkboxes are only saved to the database on node submission,
  thus only one server request for multiple new terms.
- No orphan terms in the vocabulary when the node with new terms was not saved.
- Only new terms which are checked, will be saved.E.g. when you add a new
  term with a typo, just uncheck the term and it will not be created
  on node submission.
